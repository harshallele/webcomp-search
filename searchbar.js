class SearchBar extends HTMLElement{

    constructor(){
        super();
        
        this.attachShadow({mode: "open"});

        //create link element and add it to the shadowRoot
        let bootstrap = document.createElement("link");
        bootstrap.setAttribute("rel","stylesheet");
        bootstrap.setAttribute("href","https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css")
        this.shadowRoot.appendChild(bootstrap);
    }

    async connectedCallback(){
        //load template
        let template = await (await fetch("template.html")).text();
        this.shadowRoot.innerHTML += template;
        
        // add event listener 
        this.shadowRoot.querySelector("#input-text").addEventListener("input",this.debounce(this.onSearchInput,200));
        
        //define property from children
        this.modules = JSON.parse(this.children[0].innerHTML);

    }

    onSearchInput(e){
        //get the parent element and its shadow root
        let context = e.target;
        let shadowRoot = context.shadowRoot;
        let suggContainer = shadowRoot.querySelector("#sugg-container");

        if(this.value && this.value != "") {
            //clear suggestions
            suggContainer.innerHTML = "";

            let searchSugg = [];
            
            let modules = Object.values(context.modules);
            
            modules.forEach(mod => {
                Object.keys(mod.views).forEach(k => {
                    
                    let searchString = `${mod.views[k].caption};${mod.caption}`

                    if(searchString.toLowerCase().indexOf(this.value.toLowerCase()) >= 0){
                        searchSugg.push({
                            caption: mod.views[k].caption,
                            key: k,
                            mainMod: mod.caption,
                        });
                    }
                })
            });

            if(searchSugg.length > 0){
                                
                searchSugg.forEach(sugg => {
                    //insert divs with style
                    let div = document.createElement("div");
                    div.setAttribute("class","px-1 py-2 search-sugg");
                    div.innerHTML = sugg.caption;

                    let mainModSm = document.createElement("small");
                    mainModSm.innerHTML = ` (${sugg.mainMod})`;
                    div.appendChild(mainModSm);
                    
                    div.addEventListener("mouseover",function(){
                        div.style.cursor = "pointer";
                        div.style.backgroundColor = "#eeeeee";
                    });

                    div.addEventListener("mouseleave",function(){
                        div.style.backgroundColor = "white";
                    })

                    suggContainer.appendChild(div);
                })
                suggContainer.style.display = "block";
            }
            else suggContainer.style.display = "none";

            
            console.log(searchSugg);
        }
        else suggContainer.style.display = "none";

        
    }
    //debounce
    debounce(fn, delay){
        let timer;
        
        return function(){
            let context = this;
            let args = arguments;

            clearTimeout(timer);
            timer = setTimeout(() => fn.apply(context,args),delay);
        }
    }
}

customElements.define('search-bar',SearchBar);